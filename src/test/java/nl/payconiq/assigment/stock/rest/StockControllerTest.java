package nl.payconiq.assigment.stock.rest;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import nl.payconiq.assigment.exception.EntityNotFoundException;
import nl.payconiq.assigment.service.command.CommandService;
import nl.payconiq.assigment.service.query.QueryService;
import nl.payconiq.assigment.stock.model.Stock;
import nl.payconiq.assigment.stock.rest.dto.StockDto;
import nl.payconiq.assigment.stock.rest.transformers.StockDtoToEntityTransformer;
import nl.payconiq.assigment.stock.rest.transformers.StockEntityToDtoTransformer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;

@RunWith(MockitoJUnitRunner.class)
public class StockControllerTest {

	@Mock
	private CommandService<Stock, StockDto> commandService;
	@Mock
	private QueryService<Stock> queryService;
	@Mock
	private StockEntityToDtoTransformer stockEntityToDtoTransformer;
	@Mock
	private StockDtoToEntityTransformer stockDtoToEntityTransformer;
	@InjectMocks
	private StockController controller;

	@Test
	public void findAll_ValidInput_Success() {
		final Pageable pageable = PageRequest.of(1, 5);
		when(queryService.findAll(eq(pageable))).thenReturn(buildStocks());
		controller.findAll(pageable);
		verify(queryService, times(1)).findAll(pageable);
	}

	@Test
	public void findOne_ValidInput_Success() {
		final Long id = 1L;
		final String name = "Stark Inc.";
		final Stock expected = Stock.builder().id(id).name(name).build();
		when(queryService.collectStock(id)).thenReturn(expected);
		when(stockEntityToDtoTransformer.apply(expected)).thenReturn(StockDto.builder().name(name).build());
		ResponseEntity<StockDto> actual = controller.findOne(id);
		assertNotNull(expected.getName());
		assertNotNull(actual.getBody().getName());
		assertEquals(expected.getName(), actual.getBody().getName());
	}

	@Test
	public void delete_ValidInput_Success() {
		final Long id = 1L;
		final Stock stock = Stock.builder().id(id).build();
		when(queryService.collectStock(id)).thenReturn(stock);
		controller.delete(id);
		verify(commandService, times(1)).delete(stock);
	}

	@Test
	public void update_ValidInput_Success() {
		final Long id = 1L;
		final Stock stock = Stock.builder().id(id).name("DC").currentPrice(BigDecimal.ONE).lockVersion(0L).lastUpdate(Instant.now()).build();
		final StockDto stockDto = StockDto.builder().name("MARVEL").currentPrice(BigDecimal.TEN).lockVersion(1L).build();
		when(queryService.collectStock(id)).thenReturn(stock);
		controller.update(id, stockDto);
		verify(commandService, times(1)).update(id, stockDto);
	}

	@Test
	public void create_ValidInput_Success() {
		final Long id = 1L;
		Stock stock = Stock.builder().id(id).build();
		final StockDto stockDto = StockDto.builder().name("GSS").build();
		HttpServletRequest request = new MockHttpServletRequest();
		when(stockDtoToEntityTransformer.apply(stockDto)).thenReturn(stock);
		when(commandService.save(stock)).thenReturn(stock);
		controller.create(stockDto, request);
		verify(commandService, times(1)).save(stock);
	}

	@Test(expected = EntityNotFoundException.class)
	public void delete_NotExistingId_NotFound() {
		final Long id = 1L;
		when(queryService.collectStock(id)).thenThrow(new EntityNotFoundException(format("Stock with id '%s' not found.", id)));
		controller.delete(id);
	}

	private Page<Stock> buildStocks() {
		List<Stock> stocks = new ArrayList<>(2);
		stocks.add(Stock.builder().name("PrivatBank").currentPrice(new BigDecimal(22.86)).build());
		stocks.add(Stock.builder().name("ING").currentPrice(new BigDecimal(86.22)).build());
		return new PageImpl<>(stocks);
	}

}