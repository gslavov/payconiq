package nl.payconiq.assigment.config.swagger;

import static com.google.common.base.Predicates.not;
import static springfox.documentation.builders.PathSelectors.regex;
import static springfox.documentation.builders.RequestHandlerSelectors.any;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	private final SwaggerConfigProperties configProperties;

	public SwaggerConfig(final SwaggerConfigProperties configProperties) {
		this.configProperties = configProperties;
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
			.select()
			.apis(any())
			.paths(not(regex("/error")))
			.build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title(configProperties.getTitle()).description(configProperties.getDescription())
			.contact(new Contact(configProperties.getContact().getName(), configProperties.getContact().getUrl(),
				configProperties.getContact().getEmail()))
			.version("1.0")
			.license("All rights reserved.")
			.build();
	}

}