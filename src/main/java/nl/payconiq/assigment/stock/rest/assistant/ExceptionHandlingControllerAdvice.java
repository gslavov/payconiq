package nl.payconiq.assigment.stock.rest.assistant;

import nl.payconiq.assigment.exception.EntityNotFoundException;
import nl.payconiq.assigment.stock.rest.StockController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.Builder;
import lombok.Getter;

@ControllerAdvice(assignableTypes = StockController.class)
public class ExceptionHandlingControllerAdvice extends ResponseEntityExceptionHandler {

	@ExceptionHandler({ EntityNotFoundException.class })
	public ResponseEntity<ErrorResponse> entityNotFoundException(final EntityNotFoundException exception) {
		return new ResponseEntity<>(ErrorResponse.builder().message(exception.getMessage()).build(), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler({ ObjectOptimisticLockingFailureException.class })
	public ResponseEntity<ErrorResponse> entityNotFoundException() {
		return new ResponseEntity<>(ErrorResponse.builder().message("Row was updated or deleted by another transaction").build(),
			HttpStatus.BAD_REQUEST);
	}

	@Getter
	@Builder
	private static class ErrorResponse {
		private final String message;
	}

}
