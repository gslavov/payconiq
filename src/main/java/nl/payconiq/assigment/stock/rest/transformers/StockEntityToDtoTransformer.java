package nl.payconiq.assigment.stock.rest.transformers;

import java.util.function.Function;

import nl.payconiq.assigment.stock.model.Stock;
import nl.payconiq.assigment.stock.rest.dto.StockDto;

import org.springframework.stereotype.Component;

@Component
public class StockEntityToDtoTransformer implements Function<Stock, StockDto> {

	@Override
	public StockDto apply(final Stock stock) {
		return StockDto.builder()
			.name(stock.getName())
			.currentPrice(stock.getCurrentPrice())
			.lockVersion(stock.getLockVersion())
			.lastUpdate(stock.getLastUpdate())
			.build();
	}

}
