package nl.payconiq.assigment.stock.repository;

import nl.payconiq.assigment.stock.model.Stock;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockRepository extends PagingAndSortingRepository<Stock, Long> {

}
