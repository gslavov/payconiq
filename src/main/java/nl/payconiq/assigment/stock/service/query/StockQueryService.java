package nl.payconiq.assigment.stock.service.query;

import static java.lang.String.format;

import java.util.Optional;

import nl.payconiq.assigment.exception.EntityNotFoundException;
import nl.payconiq.assigment.service.query.QueryService;
import nl.payconiq.assigment.stock.model.Stock;
import nl.payconiq.assigment.stock.repository.StockRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class StockQueryService implements QueryService<Stock> {

	private final StockRepository repository;

	@Override
	public Optional<Stock> findOne(final Long id) {
		return repository.findById(id);
	}

	@Override
	public Page<Stock> findAll(final Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Stock collectStock(final Long id) {
		return this.findOne(id).orElseThrow(() -> new EntityNotFoundException(format("Stock with id '%s' not found.", id)));
	}

}
