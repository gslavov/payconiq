package nl.payconiq.assigment.service.query;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface QueryService<T> {

	Optional<T> findOne(Long id);

	Page<T> findAll(Pageable pageable);

	T collectStock(Long id);

}
