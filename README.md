Result of usecase: no job offer (2019)  
Reason why: ![](screenshot/email-con.png)

# Payconiq Assignment - Backend Developer

A java based backend application using REST.
The list of stocks are created in memory on application startup. 

## Build/Run the Application
To build and run the application: 
* For Windows: `mvn clean package && java -jar target\assigment-0.1.0-SNAPSHOT.jar`  
* For others: `mvn clean package docker:run`  

## API
###### _note that port might be different if environment variable PORT is defined._
To access the Rest API's:  
[Swagger-UI](http://localhost:8080/swagger-ui.html)  

## Future improvements
I time-boxed myself to 5 hours when implementing Payconiq Assignment.
In case I was going to spend twice more time on it following improvements could be done:
1. Create a front-end which shows the stock list.
2. Add a health-check endpoint (spring actuator dependency)
3. Improve test coverage
5. Internationalization of validation messages.

